jQuery(document).ready(function(event){
  var isAnimating = false,
    newLocation = '';
    firstLoad = false;
  
  //trigger smooth transition from the actual page to the new one 
  jQuery('.wrapper').on('click', '[data-type="page-transition"]', function(event){
    event.preventDefault();
    //detect which page has been selected
    var newPage = jQuery(this).attr('href');
    //if the page is not already being animated - trigger animation
    if( !isAnimating ) changePage(newPage, true);
    firstLoad = true;
  });

  //detect the 'popstate' event - e.g. user clicking the back button
  jQuery(window).on('popstate', function() {
  	if( firstLoad ) {
      /*
      Safari emits a popstate event on page load - check if firstLoad is true before animating
      if it's false - the page has just been loaded 
      */
      var newPageArray = location.pathname.split('/'),
        //this is the url of the page to be loaded 
        newPage = newPageArray[newPageArray.length - 1];

      if( !isAnimating  &&  newLocation != newPage ) changePage(newPage, false);
    }
    firstLoad = true;
	});
	 loadJS = function(src) {
		 var jsLink = jQuery("<script type='text/javascript' src='"+src+"'>");
		 jQuery("body").append(jsLink); 
	 };
   loadCSS = function(src) {
     var cssLink = jQuery("<link rel='stylesheet' href='"+src+"'>");
     jQuery("body").append(cssLink); 
   }; 
	function changePage(url, bool) {
    isAnimating = true;
     jQuery("script[src='js/vertical-slider.js']").remove();
	 jQuery("script[src='js/custom.js']").remove();
     jQuery(".page").removeClass('in-view');
	 jQuery(".page").removeClass('in-view-active');

    // trigger page animation
    jQuery('body').addClass('page-is-changing');
	
		jQuery('.cd-loading-bar').css({opacity: 0.0, visibility: "visible"}).animate({opacity: 1.0, width: "100%"});
		
    jQuery('.cd-loading-bar').one('webkitTransitionEnd otransitionend oTransitionEnd msTransitionEnd transitionend', function(){
    	loadNewContent(url, bool);
      newLocation = url;
     jQuery('.cd-loading-bar').off('webkitTransitionEnd otransitionend oTransitionEnd msTransitionEnd transitionend');
    });
    //if browser doesn't support CSS transitions
    if( !transitionsSupported() ) {
      loadNewContent(url, bool);
      newLocation = url;
    }
	}

	function loadNewContent(url, bool) {
		url = ('' == url) ? 'index.html' : url;
  	var newSection = 'page-'+url.replace('.html', '');
  	var section = jQuery('<div class="main-content '+newSection+'"></div>');  
  	section.load(url+' .main-content > *', function(event){
      // load new content and replace <main> content with the new one
      jQuery('main').html(section);
      //if browser doesn't support CSS transitions - dont wait for the end of transitions
      var delay = ( transitionsSupported() ) ? 1200 : 0;
      setTimeout(function(){
        //wait for the end of the transition on the loading bar before revealing the new content
        ( section.hasClass('page-index') ) ? jQuery('body').addClass('page-index') : jQuery('body').removeClass('page-index');
		( section.hasClass('page-about') ) ? jQuery('body').addClass('page-about') : jQuery('body').removeClass('page-about');
		( section.hasClass('page-services') ) ? jQuery('body').addClass('page-services') : jQuery('body').removeClass('page-services');
		( section.hasClass('page-career') ) ? jQuery('body').addClass('page-career') : jQuery('body').removeClass('page-career');
		( section.hasClass('page-login') ) ? jQuery('body').addClass('page-login') : jQuery('body').removeClass('page-login');
		( section.hasClass('page-our-work') ) ? jQuery('body').addClass('page-our-work') : jQuery('body').removeClass('page-our-work');
		( section.hasClass('page-media-center') ) ? jQuery('body').addClass('page-media-center') : jQuery('body').removeClass('page-media-center');
		( section.hasClass('page-contact-us') ) ? jQuery('body').addClass('page-contact-us') : jQuery('body').removeClass('page-contact-us');
		
        jQuery('body').removeClass('page-is-changing');
        jQuery('.cd-loading-bar').one('webkitTransitionEnd otransitionend oTransitionEnd msTransitionEnd transitionend', function(){
          isAnimating = false;
          jQuery('.cd-loading-bar').off('webkitTransitionEnd otransitionend oTransitionEnd msTransitionEnd transitionend');
        });
		
		var t = jQuery('.page').attr("data-meta-title");
		document.title = t;
		
        if( !transitionsSupported() ) isAnimating = false;
		 loadJS("js/custom.js");
		
		 if(url =='our-work.html'){      
  		  loadJS("js/jquery.mCustomScrollbar.js"); 
        loadCSS("css/jquery.mCustomScrollbar.css");
        jQuery(".Scroll-content").mCustomScrollbar({
          theme:"inset-2"
        });
        loadJS("js/vertical-slider.js"); 
        loadJS("js/hammer.min.js"); 
        verticalSlider.init();      
        // $('#imgGlr').verticalSlider({
        //   // Element on which informational classes will be put (current section index, last section...)
        //   infoSelector: 'html',
        //   // Autoplay functionality
        //   autoplay: false
        // });
     }
     if(url =='media-center.html'){
      // loadJS("js/bootstrap.js");
          loadJS("js/jquery.mCustomScrollbar.js"); 
          loadCSS("css/jquery.mCustomScrollbar.css");
          jQuery(".Scroll-content").mCustomScrollbar({
            theme:"inset-2"
          });          

          jQuery(".nav-tabs li").click(function(){
            jQuery(".nav-tabs li").removeClass("active");
            jQuery(this).addClass("active");            
          });
         
          jQuery(".news-nav").click(function () {            
            jQuery(".tab-pane").removeClass('in');
            jQuery(".tab-pane").removeClass('active');
            jQuery("#newsTab").addClass('in');
            jQuery("#newsTab").addClass('active');
          });
          jQuery(".press-nav").click(function () {  
          jQuery(".tab-pane").removeClass('in');
            jQuery(".tab-pane").removeClass('active');          
            jQuery("#pressTab").addClass('in');
            jQuery("#pressTab").addClass('active');                     
          });
          jQuery(".media-nav").click(function () {
            jQuery(".tab-pane").removeClass('in');
            jQuery(".tab-pane").removeClass('active');
            jQuery("#mediaTab").addClass('in');
            jQuery("#mediaTab").addClass('active');
          });
          jQuery(".home-nav").click(function () {
            jQuery(".tab-pane").removeClass('in');
            jQuery(".tab-pane").removeClass('active');
            jQuery("#home").addClass('in');
            jQuery("#home").addClass('active');
          });

     }
			window.setTimeout(function(){jQuery(".page").addClass('in-view');}, 500);
			jQuery('.cd-loading-bar').css({opacity: 0.0,visibility: 'hidden'}).animate({opacity:0});

      }, delay);

      
      if(url!=window.location && bool){
        //add the new page to the window.history
        //if the new page was triggered by a 'popstate' event, don't add it
        window.history.pushState({path: url},'',url);
      }
	});
		
  }

  function transitionsSupported() {
    return jQuery('html').hasClass('csstransitions');
  }
});

