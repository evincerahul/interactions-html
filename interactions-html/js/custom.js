/*******************************  START Function   ******************************************/
jQuery(document).ready(function(event) {
    // menu
    var menuOpen = false;
    var contactOpen = false;    
    jQuery('.toggle').bind('click', function(event) {                
        if (menuOpen == false) {
			jQuery('.tabContent').hide();			                        
            jQuery('.newsContent').hide();          
            jQuery('.toggle').addClass('open');
            jQuery('.menu').fadeIn('slow', function() {
                menuOpen = true;
            });
        }

        if (menuOpen == true) {            
            jQuery('.toggle').removeClass('open');
            jQuery('.menu').fadeOut('slow', function() {
                menuOpen = false;
            });
        }
    });

    jQuery('.navigation li a').bind('click', function(event) {
        jQuery('.navigation li').removeClass('active');
        jQuery(this).parent('li').addClass('active');
        if (menuOpen == true) {
            jQuery('.toggle').removeClass('open');
            jQuery('.menu').fadeOut('slow', function() {
                menuOpen = false;
            });
        }
    });

    /**********************************************/
    jQuery('.toggle1').bind('click', function(event) {
        if (contactOpen == false) {
            jQuery('.toggle1').addClass('open');
            jQuery('.contact').fadeIn('slow', function() {
                contactOpen = true;
            });
        }
        if (contactOpen == true) {
            jQuery('.toggle1').removeClass('open');
            jQuery('.contact').fadeOut('slow', function() {
                contactOpen = false;
            });
        }
    });
    /****  OUR WORK PAGE JS ****/

    // jQuery code for our work home tab
    jQuery('#our-work-tab a.linkButton').click(function(event) {
	    jQuery('#our-work-tab a.linkButton').removeClass('open');
	    jQuery('.tabContent').fadeOut('slow');
	    var dataID = jQuery(this).attr('data-id');	    
	    jQuery('#ourwork-'+dataID).addClass('open');
	    jQuery('#ourworkContent-'+dataID).fadeIn('slow');
	    contactOpen = true;
	});   
    /**** Media Center page js *****/  	
	
    // jQuery code for News tab
    jQuery('#newsTab a.linkButton').click(function(event) {
	    jQuery('#newsTab a.linkButton').removeClass('open');
	    jQuery('.newsContent').fadeOut('slow');
	    var dataID = jQuery(this).attr('data-id');	    
	    jQuery('#news-'+dataID).addClass('open');
	    jQuery('#newsContent-'+dataID).fadeIn('slow');
	    contactOpen = true;
	});

	// jQuery code for Press tab
    jQuery('#pressTab a.linkButton').click(function(event) {
	    jQuery('#pressTab a.linkButton').removeClass('open');
	    jQuery('.newsContent').fadeOut('slow');
	    var dataID = jQuery(this).attr('data-id');	    
	    jQuery('#press-'+dataID).addClass('open');
	    jQuery('#pressContent-'+dataID).fadeIn('slow');
	    contactOpen = true;
	});


    jQuery('.nav-tabs li').click(function(event) {
        if (contactOpen == true) {
             jQuery('.newsContent').hide();
             jQuery('.tabContent').hide();
        }
    });

	// jQuery code for Publications tab
    jQuery('#home a.linkButton').click(function(event) {
	    jQuery('#home a.linkButton').removeClass('open');
	    jQuery('.tabContent').fadeOut('slow');
	    var dataID = jQuery(this).attr('data-id');	    
	    jQuery('#publications-'+dataID).addClass('open');
	    jQuery('#publicationsContent-'+dataID).fadeIn('slow');
	    contactOpen = true;
	});





    /******************   Our Work Image Gallery Popup **************************/

    jQuery('.image-gallery').click('click', function() {
            jQuery('.image-gallery').addClass('open');
            jQuery('.image-gallery-content').fadeIn('slow', function() {
                contactOpen = true;
            });
			/*jQuery("script[src='js/vertical-slider.js']").remove();
			  window.setTimeout(function() {
        		loadJS("js/vertical-slider.js"); 
    		}, 500);*/
			
			
     });

    jQuery('.image-gallery2').click('click', function() {
            jQuery('.image-gallery2').addClass('open');
            jQuery('.jamba-gallery').fadeIn('slow', function() {
                contactOpen = true;
            });
     });

    /******************   Our Work video Gallery Popup **************************/

    jQuery('.video-gallery').click('click', function() {
            jQuery('.video-gallery').addClass('open');
            jQuery('.video-gallery-content').fadeIn('slow', function() {
                contactOpen = true;
            });
     });


    /***********************************/
    jQuery('.cancel-btn').bind('click', function(event) {
        if (contactOpen == true) {
            jQuery('.open').removeClass('open');
            jQuery(this).parent().fadeOut('slow', function() {
                contactOpen = false;
            });
            jQuery('.tabContent').hide();
        }
    });

   /************* For video controls ***************************/ 

    if (jQuery("video").hasClass('heroVideo')) {
        /* For video controls */
        var vid = document.getElementById("bgvid");
        vid.addEventListener('ended', function() {
            // only functional if "loop" is removed 
            vid.pause();
            // to capture IE10
            vidFade();
        });
        jQuery('.onBtn').click(function() {
            vid.play();
            jQuery('.videoButtons span').removeClass('active');
            jQuery(this).addClass('active');
            jQuery('#bgvid').removeClass('fade').addClass('animation');
            jQuery('.HeroImage').addClass('fade').removeClass('animation');
        });
        jQuery('.offBtn').click(function() {
            vid.pause();
            jQuery('.videoButtons span').removeClass('active');
            jQuery('#bgvid').addClass('fade').removeClass('animation');
            jQuery('.HeroImage').removeClass('fade').addClass('animation');
            jQuery(this).addClass('active');
        });
    }

    /************************* Our Work video controls   **********************************/


    jQuery('.on-off').click(function() {
        if (vid.paused == false) {
            vid.pause();
            jQuery('#bgvid').addClass('fade').removeClass('animation');
            jQuery('.HeroImage').removeClass('fade').addClass('animation');
            jQuery(this).removeClass('active');

        } else {
            vid.play();
            jQuery('#bgvid').removeClass('fade').addClass('animation');
            jQuery('.HeroImage').addClass('fade').removeClass('animation');
            jQuery(this).addClass('active');

        }
    });

    jQuery('.page-our-work .cancel-btn').click(function() {
            vid.pause();
            jQuery('.page-our-work .videoButtons span').removeClass('active');
            jQuery('.page-our-work #bgvid').addClass('fade').removeClass('animation');
            jQuery('.page-our-work .HeroImage').removeClass('fade').addClass('animation');
            jQuery(this).addClass('active');
        });

});

/*******************************  End Functoin   ******************************************/

jQuery(document).ready(function() {
    window.setTimeout(function() {
        jQuery(".page").addClass('in-view');
    }, 1200);

    window.setTimeout(function() {
        jQuery(".page").addClass('in-view-active');
    }, 1400);

    //  jQuery(".main-container").addClass('in-view').delay(5000);
    window.setTimeout(function() {
        jQuery(".cd-loading-bar").css({
            width: "0%"
        });
    }, 1200);
});
/****************** VIDEO Auto play ************************/
